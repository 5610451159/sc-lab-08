package control;

import java.util.ArrayList;

import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;
import interfaces.Measurable;
import interfaces.Taxable;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test test = new Test() ;
		
		test.testPerson() ;
		System.out.println("\n") ;
		test.testMin() ;
		System.out.println("\n") ;
		test.testTax() ;
	}
	
	public void testPerson() {
		Measurable[] person = new Measurable[5] ;
		person[0] = new Person("noey", 154, 4600) ;
		person[1] = new Person("khanoon", 159, 1400) ;
		person[2] = new Person("son", 130, 800) ;
		person[3] = new Person("papa", 166, 15000) ;
		person[4] = new Person("mama", 156, 5000) ;
		
		System.out.println("-- Test Person --") ;
		System.out.println("Average height : " + Data.average(person) + " cm.") ;
	}
	
	public void testMin() {
		Measurable[] person = new Measurable[2] ;
		Measurable[] p = new Measurable[1] ;
		person[0] = new Person("noey", 154, 4600) ;
		person[1] = new Person("mek", 175, 6400) ;
		p[0] = Data.min(person[0], person[1]) ;
		
		Measurable[] bankaccount = new Measurable[2] ;
		Measurable[] b = new Measurable[1] ;
		bankaccount[0] = new BankAccount("noey", 35700) ;
		bankaccount[1] = new BankAccount("mek", 4500) ;
		b[0] = Data.min(bankaccount[0], bankaccount[1]) ;
		
		Measurable[] country = new Measurable[2] ;
		Measurable[] c = new Measurable[1] ;
		country[0] = new Country("Thailand", 10000) ;
		country[1] = new Country("China", 50000) ;
		c[0] = Data.min(country[0], country[1]) ;
		
		System.out.println("-- Test Min --") ;
		System.out.println("Person : " + Data.average(p) + " cm.") ;
		System.out.println("BankAccount : " + Data.average(b) + " bath.") ;
		System.out.println("Country : " + Data.average(c) + " person") ;
	}
	
	public void testTax() {
		ArrayList<Taxable> person = new ArrayList<Taxable>() ;
		person.add(new Person("noey", 154, 4600)) ;
		
		ArrayList<Taxable> company = new ArrayList<Taxable>() ;
		company.add(new Company("MN", 500000, 100000)) ;
		
		ArrayList<Taxable> product = new ArrayList<Taxable>() ;
		product.add(new Product("food", 1000)) ;
		
		ArrayList<Taxable> taxable = new ArrayList<Taxable>() ;
		taxable.add(new Person("noey", 154, 4600)) ;
		taxable.add(new Company("MN", 500000, 100000)) ;
		taxable.add(new Product("food", 1000)) ;
		
		System.out.println("-- Test Tax --") ;
		System.out.println("Person : pay for tax " + TaxCalculator.sum(person) + " bath.");
		System.out.println("Company : pay for tax " + TaxCalculator.sum(company) + " bath.");
		System.out.println("Product : pay for tax " + TaxCalculator.sum(product) + " bath.");
		System.out.println("All : pay for tax " + TaxCalculator.sum(taxable) + " bath.");
	}
}
