package model;

import interfaces.Taxable;

public class Company implements Taxable {
	String name ;
	double income ;
	double expense ;

	public Company(String name, double income, double expense){
		this.name = name ;
		this.income = income ;
		this.expense = expense ;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0.0 ;
		
		tax = (income - expense) * 0.3 ;
		
		return tax ;
	}

}
