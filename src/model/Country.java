package model;

import interfaces.Measurable;

public class Country implements Measurable {
	String name ;
	double people ;
	
	public Country(String name, double people) {
		this.name = name ;
		this.people = people ;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.people ;
	}
}
