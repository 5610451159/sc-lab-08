package model;

import interfaces.Measurable;

public class Data {
	
	public static double average(Measurable[] measurable) {
		double sum = 0.0 ;
		double sumheight = 0.0 ;
		int lenght = measurable.length ;
		for (Measurable m : measurable) {
			sum += m.getMeasure() ;
		}
		
		if (lenght > 1) {
			sumheight = sum/lenght ;
		}
		
		else {
			sumheight = sum ;
		}
		
		return sumheight ;
	}
	
	public static Measurable min(Measurable m1, Measurable m2) {
		if (m1.getMeasure() < m2.getMeasure()) {
			return m1 ;
		}
		
		else {
			return m2 ;
		}
	}
}
