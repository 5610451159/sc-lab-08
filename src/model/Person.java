package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable, Taxable {
	String name ;
	double height ;
	double annualincome ;
	
	public Person(String name, double height, double annualincome) {
		this.name = name ;
		this.height = height ;
		this.annualincome = annualincome ;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.height ;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0.0 ;
		
		if (this.annualincome <= 300000) {
			tax = this.annualincome * 0.05 ;
		}
		
		else {
			tax = ((300000 * 0.05) + ((this.annualincome - 300000) * 0.1)) ;
		}
		
		return tax ;
	}
}
