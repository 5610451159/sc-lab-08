package model;

import interfaces.Taxable;

public class Product implements Taxable {
	String name ;
	double price ;
	
	public Product(String name, double price){
		this.name = name ;
		this.price = price ;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double tax = 0.0 ;
		
		tax = price * 0.07 ;
		
		return tax ;
	}

}
