package model;

import interfaces.Taxable;
import java.util.ArrayList;

public class TaxCalculator {
	
	public static double sum(ArrayList<Taxable> taxList) {
		double sumtax = 0.0 ;
		
		for (Taxable t : taxList) {
			sumtax += t.getTax() ;
		}
		
		return sumtax ;
	}
}
